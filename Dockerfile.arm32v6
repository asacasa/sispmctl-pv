FROM alpine as qemu

RUN if [ -n "arm" ]; then \
		wget -O /qemu-arm-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-arm-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-arm-static; \
	fi; \
	chmod a+x /qemu-arm-static

FROM arm32v6/alpine:3.13 as builder

COPY --from=qemu /qemu-arm-static /usr/bin/

# INSTALL RUNTIME DEPENDENCIES

ADD sispmctl-mirror /work

RUN apk add build-base libusb-compat-dev autoconf automake libtool

WORKDIR /work

RUN sh autogen.sh; ./configure; make -j 8; make install DESTDIR=/tmp/destdir 

FROM arm32v6/alpine:3.13

COPY --from=qemu /qemu-arm-static /usr/bin/

# COPY ARTIFACTS FROM OTHER CONTAINERS

RUN apk add libusb-compat screen

COPY --from=builder /tmp/destdir/usr/local/bin/* /usr/local/bin/
COPY --from=builder /tmp/destdir/usr/local/lib/* /usr/local/lib/
COPY --from=builder /tmp/destdir/usr/local/share/doc/sispmctl/* /usr/local/share/doc/sispmctl/

# CONFIGURATION

CMD [ "/sbin/init" ]
